<?php

// Conexão com o banco de dados
$servername = "sql458.main-hosting.eu";
$username = "u150922623_curitba";
$password = "tgHNDYBNz3T/";
$dbname = "u150922623_curitba";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verifica a conexão
if ($conn->connect_error) {
    die("Falha na conexão: " . $conn->connect_error);
}

// Consulta SQL
$sql = "
    SELECT DISTINCT dates.date + INTERVAL hours.hour HOUR + INTERVAL minutes.minute MINUTE AS agendamento
    FROM
    (
        SELECT CURRENT_DATE() - INTERVAL WEEKDAY(CURRENT_DATE()) DAY + INTERVAL 0 DAY AS date
        UNION ALL
        SELECT CURRENT_DATE() - INTERVAL WEEKDAY(CURRENT_DATE()) DAY + INTERVAL 1 DAY
        UNION ALL
        SELECT CURRENT_DATE() - INTERVAL WEEKDAY(CURRENT_DATE()) DAY + INTERVAL 2 DAY
        UNION ALL
        SELECT CURRENT_DATE() - INTERVAL WEEKDAY(CURRENT_DATE()) DAY + INTERVAL 3 DAY
        UNION ALL
        SELECT CURRENT_DATE() - INTERVAL WEEKDAY(CURRENT_DATE()) DAY + INTERVAL 4 DAY
    ) AS dates
    CROSS JOIN
    (
        SELECT 8 AS hour
        UNION ALL
        SELECT 9
        UNION ALL
        SELECT 10
        UNION ALL
        SELECT 11
        UNION ALL
        SELECT 12
        UNION ALL
        SELECT 13
        UNION ALL
        SELECT 14
        UNION ALL
        SELECT 15
        UNION ALL
        SELECT 16
    ) AS hours
    CROSS JOIN
    (
        SELECT 0 AS minute
        UNION ALL
        SELECT 30
    ) AS minutes
    LEFT JOIN tabela ON dates.date + INTERVAL hours.hour HOUR + INTERVAL minutes.minute MINUTE = tabela.agendamento
    WHERE WEEKDAY(dates.date) BETWEEN 0 AND 4
    AND tabela.agendamento IS NULL
    AND dates.date + INTERVAL hours.hour HOUR + INTERVAL minutes.minute MINUTE > DATE_ADD(NOW(), INTERVAL -3 HOUR)
    AND (hours.hour < 16 OR (hours.hour = 16 AND minutes.minute = 0) OR (hours.hour = 16 AND minutes.minute = 30));
";

$result = $conn->query($sql);

// Array para armazenar os resultados
$agendamentos = array();

if ($result->num_rows > 0) {
    // Output data of each row
    while($row = $result->fetch_assoc()) {
        $agendamentos[] = $row['agendamento'];
    }
} else {
    echo "Nenhum agendamento disponível.";
}

// Encerra a conexão com o banco de dados
$conn->close();

// Retorna os resultados como JSON
header('Content-Type: application/json');
echo json_encode($agendamentos);
?>