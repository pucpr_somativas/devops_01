<!DOCTYPE html>
<html lang="pt-BR">
    
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos.css">
    <title>Curitba Pet Care</title>
</head>

<?php    
    $url = "http://localhost/somativa01/api.php";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $return_api = json_decode($response, True);
?>


<body>
    <div id="logo" class="logo-div">
        <img src="logo.jpeg" alt="Logo Curitba Pet Care" class="logo">
    </div>
    <hr>
    <h1>
        Agendar horário na Curitba
    </h1>
    <div class="formulario-div">
        <form action="action.php" method="POST">
            <h2>Formulário de Agendamento</h2>
            <div class="form-group">
                <div class="form-wrapper">
                    <label for="">Nome do tutor</label>
                    <input type="text" class="form-control" id="tutor" name="tutor">
                </div>
                <div class="form-wrapper">
                    <label for="">Nome do animal</label>
                    <input type="text" class="form-control" id="animal" name="animal">
                </div>
            </div>
            <div class="form-wrapper">
                <label for="agendamento">Data e horário do banho:</label>
                <select name="data-hora" id="data-hora" class="form-control">
                <?php
                    $texto = "";
                    foreach($return_api as $horario){                
                        $texto = $texto."<option value=".'"'.$horario.'"'.">".date("d/m/Y - H:i", strtotime($horario))."</option>";
                    }
                    echo $texto;
                ?>                   
                </select>
            </div>
            <div class="form-wrapper">
                <label for="cars">Porte do animal:</label>
                    <select name="porte" id="porte" class="form-control">
                    <option value="pequeno">Pequeno</option>
                    <option value="medio">Médio</option>
                    <option value="grande">Grande</option>                    
                </select>
            </div>            
            </div>
            <button>Agendar</button>
        </form>
    </div>
</body>
</html>
